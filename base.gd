extends Node

var camera = null
var cam_pos = Vector3()
var active_player = null
var label
var screen_size
const base_size = Vector2(1024,600)

func _enter_tree():
	screen_size = get_viewport().size

	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	get_tree().get_root().connect("size_changed", self, "screen_resized")

func _ready():
	debug_operation()


func screen_resized():
	screen_size = get_viewport().size
	if active_player != null:
		active_player.resize()

func _process(delta):#services
	cam_pos = get_viewport().get_camera().global_transform.origin


func debug_operation():
#	var base = 
	return
	var room = get_node("/root/main/benchnode")
	if room == null:
		print("error")
		return
	for i in room.get_children():
		var new = load("res://nwentity/entity.tscn").instance()
		new.name = i.name
		new.global_transform = i.global_transform
		room.add_child(new)
		new.set_owner(room)
		i.queue_free()
#