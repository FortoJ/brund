extends KinematicBody
var view_old = "fn"
var view_new = "fn"
var anim_old = "idle"
var anim_new = "idle"
var view_at_left = false
var my_dir = Vector3()

#View Height rules
#const VH_TOP_LIMIT = 1.2
#const CAMERA_HEIGHT = 2
#var vh_old = false

#const statedb ={"front":0
#				,"frontside":1
#				,"side":2
#				,"backside":3
#				,"back":4
#				}

#const facedba ={0:"front"
#				,1:"frontside"
#				,2:"side"
#				,3:"backside"
#				,4:"back"
#				}

#const viewdb = ["front","frontside","side","backside","back"]
#const viewdb = ["backside","side","frontside","front","frontside","side","backside","back"]
#const viewdb = ["back","backside","side","frontside","front","frontside","side","backside"]
#const viewdb = ["side","frontside","front","frontside","side","backside","back","backside"]
const viewdb = ["sd","fnsd","fn","fnsd","sd","bksd","bk","bksd"]
#sd:side, fnsd:front-side, fn:front, bksd:backside, bk:back
var camera_watching




func _ready():
	camera_watching = get_viewport().get_camera()

func change_view(view):
	var to_play = str(view,"_",anim_old)
	$anim.play(to_play)
	$sprite.flip_h = view_at_left




func split_pos(who):
	var pos = who.global_transform.origin
	return [Vector2(pos.x, pos.z), pos.y]

func _process(delta):
	var msg = ""
	rotation.y += 1*delta#testing propouse
#	global_transform = global_transform.looking_at(get_viewport().get_camera().global_transform.origin, Vector3(0,1,0))
	
	my_dir = rotation_degrees.y#get my degree
	var cam_dir = [Vector2(base.cam_pos.x, base.cam_pos.z),Vector2(transform.origin.x,transform.origin.z)]
	cam_dir = rad2deg(cam_dir[1].angle_to_point(cam_dir[0]))+180#degree relative to world position
	msg = str("camdir: ",base.cam_pos)

	var face_dir = (my_dir+cam_dir+22)/360#additionate both degree (entity face direction relative to camera)


	face_dir -= int(face_dir)#remove the number of turns and keep only the current rotation value
	face_dir = int(face_dir*8)#split the float rotation in 8 direction and get only it's integer
	
	view_at_left = (face_dir < 2) or (face_dir == 7)#these direction are facing the left side of the camera

	view_new = viewdb[face_dir]#


	if (view_new != view_old) or (anim_old!= anim_new):
		change_view(view_new)
		view_old = view_new
		anim_old = anim_new
	
	
#	camera_watching.get_node("Label").text = msg