extends Sprite3D
const TIME = 4
const path = "res://draft/fx/"
var timer = 0
var db_size
var db_pos = 0
var fx_db = [
	["1_magicspell_spritesheet.png",9,9,75],
	["2_magic8_spritesheet.png",8,8,61],
	["3_bluefire_spritesheet.png",8,8,61],
	["4_casting_spritesheet.png",9,9,73],
	["5_magickahit_spritesheet.png",7,7,41],
	["6_flamelash_spritesheet.png",7,7,46],
	["7_firespin_spritesheet.png",8,8,61],
	["8_protectioncircle_spritesheet.png",8,8,61],
	["9_brightfire_spritesheet.png",8,8,61],
	["10_weaponhit_spritesheet.png",6,6,31],
	["11_fire_spritesheet.png",8,8,61],
	["12_nebula_spritesheet.png",8,8,61],
	["13_vortex_spritesheet.png",8,8,61],
	["14_phantom_spritesheet.png",8,8,61],
	["15_loading_spritesheet.png",11,11,121],
	["16_sunburn_spritesheet.png",8,8,61],
	["17_felspell_spritesheet.png",10,10,91],
	["18_midnight_spritesheet.png",8,8,61],
	["19_freezing_spritesheet.png",10,10,96],
	["20_magicbubbles_spritesheet.png",8,8,61]
	]

func _enter_tree():
	db_size = fx_db.size()
	var animation = $anim.get_animation("fx")
	var track = animation.find_track(".:frame")
	#print(str("track is: ",animation.find_track(".:frame") ) )
	print(animation.track_get_key_value(0,1) )
	#track_set_key_value( int idx, int key, Variant value )

func switch_fx():
	db_pos+= 1
	if db_pos > db_size-1:
		db_pos = 0
	print(fx_db[db_pos][0])
	var sprite = fx_db[db_pos]
	texture = load(str(path,sprite[0]))
	vframes = sprite[1]
	hframes = sprite[2]
	$anim.get_animation("fx").track_set_key_value( 0,1, sprite[3])
	

func _process(delta):
	timer -= 1*delta
	if timer <= 0:
		switch_fx()
		timer = TIME
#		base.label.text = str(randi())