extends KinematicBody
var view_old = "front"
var view_new = "front"
var view_at_left = false
var my_dir = Vector3()

#View Height rules
const VH_TOP_LIMIT = 1.2
const CAMERA_HEIGHT = 2
var vh_old = false

#const statedb ={"front":0
#				,"frontside":1
#				,"side":2
#				,"backside":3
#				,"back":4
#				}

#const facedba ={0:"front"
#				,1:"frontside"
#				,2:"side"
#				,3:"backside"
#				,4:"back"
#				}

#const viewdb = ["front","frontside","side","backside","back"]
#const viewdb = ["backside","side","frontside","front","frontside","side","backside","back"]
#const viewdb = ["back","backside","side","frontside","front","frontside","side","backside"]
const viewdb = ["side","frontside","front","frontside","side","backside","back","backside"]

var camera_watching


#func _enter_tree():
#	camera_watching = base.camera

func _ready():
	camera_watching = get_viewport().get_camera()

func change_view(view):

	for spr in $from_ground.get_children():
		if spr.name != view:
			spr.hide()
		else:
			spr.show()
			spr.flip_h = view_at_left

	for spr in $from_up.get_children():
		if spr.name != view:
			spr.hide()
		else:
			spr.show()
			spr.flip_h = view_at_left




func split_pos(who):
	var pos = who.global_transform.origin
	return [Vector2(pos.x, pos.z), pos.y]

func _process(delta):
	rotation.y += 1*delta

	my_dir = rotation_degrees.y#get my degree


	var cam_pos = split_pos(camera_watching)#.global_transform.origin
	var my_pos = split_pos(self)


	var cam_angle = rad2deg( my_pos[0].angle_to_point(cam_pos[0]) )+180#2d-angle (ground)
	var height_angle = atan2(cam_pos[1]-my_pos[1],(my_pos[0]-cam_pos[0]).length())
	var dist = (my_pos[0]-cam_pos[0]).length()
	var height = cam_pos[1]-my_pos[1]-CAMERA_HEIGHT

	var vh_new = height_angle>VH_TOP_LIMIT


	var cam_dir = [Vector2(base.cam_pos.x, base.cam_pos.z),Vector2(transform.origin.x,transform.origin.z)]
	cam_dir = rad2deg(cam_dir[1].angle_to_point(cam_dir[0]))+180#degree relative to world position


	var face_dir = (my_dir+cam_dir+22)/360#additionate both degree (entity face direction relative to camera)
	face_dir -= int(face_dir)
	face_dir *=8
	face_dir = int(face_dir)
	view_at_left = (face_dir < 2) or (face_dir == 7)
	view_new = viewdb[face_dir]

#	base.active_player.label.text = str(face_dir)
	if view_new != view_old:
#		print(str("cahnge to", face_dir))
		change_view(view_new)
		view_old = view_new
	if vh_old != vh_new:
		vh_old = vh_new
		$from_up.visible = vh_old
		$from_ground.visible = !vh_old
		

