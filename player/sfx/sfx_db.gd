extends Node


var audio_lib = []
var parent
func _enter_tree():
	parent.audio_lib["exclamation-ah"] = preload("res://player/sfx/exclamation-ah.wav")
	parent.audio_lib["exclamation-grabhold2"] = preload("res://player/sfx/exclamation-grabhold2.wav")
	parent.audio_lib["exclamation-grabhold"] = preload("res://player/sfx/exclamation-grabhold.wav")
	parent.audio_lib["exclamation-jump"] = preload("res://player/sfx/exclamation-jump.wav")
	parent.audio_lib["run_solid"] = preload("res://player/sfx/run_solid.wav")
	parent.audio_lib["runstop_solid"] = preload("res://player/sfx/runstop_solid.wav")
	parent.audio_lib["voicetough-01"] = preload("res://player/sfx/voicetough-01.wav")
	parent.audio_lib["voicetough-02"] = preload("res://player/sfx/voicetough-02.wav")
	parent.audio_lib["voicetough-03"] = preload("res://player/sfx/voicetough-03.wav")
	parent.audio_lib["voicetough-04"] = preload("res://player/sfx/voicetough-04.wav")
	parent.audio_lib["voicetough-05"] = preload("res://player/sfx/voicetough-05.wav")
	parent.audio_lib["voicetough-06"] = preload("res://player/sfx/voicetough-06.wav")
	parent.audio_lib["voicetough-07"] = preload("res://player/sfx/voicetough-07.wav")
	parent.audio_lib["voicetough-08"] = preload("res://player/sfx/voicetough-08.wav")
	parent.audio_lib["voicetough-09"] = preload("res://player/sfx/voicetough-09.wav")
	parent.audio_lib["run"] = preload("res://player/sfx/run.ogg")
	queue_free()
