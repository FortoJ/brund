extends KinematicBody


const g = -50
const MAX_SPEED = 20
var speed_mult = 1#dynamic value
var vel = Vector3()
var mouse_speed = 0.004
const pitch_limit = Vector2(-1.6,1.6)

var fall_timer = 0
const fall_delay = 0.15

const SPD_WALK = 20
const SPD_RUN = 10
const JUMP_STR = 17
const ACCEL= 6
const DEACCEL= 10

#input var
var walk_side = 0#left or right
var walk_dir = 0#back or forth
var dead_zone = 0.5
var unslide = true
var input_freeze = 0

#jukebox
const SFX_DELAY = 0.05

const SFX_PATH = "res://player/sfx/"
#var sound_library = {}
var audio_lib = {}
var sfx_old = "silence"
var sfx_new = ["silence",0]#sfx request and delay
var voice_old = "silence"
var voice_new = ["silence",0]#sfx request and delay
var ground = "solid"#some sfx (mostly step based) change on ground basis
##CLIMBING MECHANICS
var climb_target = Vector3()




##NODES
var arms
var anim
var pitch
var camera
#const CAMERA_SPD = 100
const CAMERA_SPD = 20

#animation
var anim_new = "idle"
var anim_old = "onair"
var anim_pitch_limits = [0,-1.2]
var transition = true

const anim_arm_y = [500,300]




var pitch_value = 0


const statedb ={"walking":0
				,"falling":1
				,"climb":2
				}
var status = 1
var old_status = 1
var hook_chance = false
var label


func _ready():
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	resize()
	anim.play("idle")
	




func _enter_tree():
	debug_stuff()
	camera = $pitch/camera
#	feet = $feet
	arms = $pitch/camera/sight/arms
	anim = $anim
	pitch = $pitch
	if base.camera == null:
		base.camera = camera
		base.active_player = self
	label = $pitch/camera/Label
	base.label = label
	#jukebox set
	load_sfxdb()
	$sfx.connect("finished",self,"clean_audio", ["sfx"])
	$voice.connect("finished",self,"clean_audio", ["voice"])

#func load_sfxdb():
#	var dir = Directory.new()
#	var contents = []
#	if dir.open(SFX_PATH) == OK:
#		dir.list_dir_begin()
#		var file = dir.get_next()
#		while (file != ""):
#			if file.ends_with("wav") or file.ends_with("ogg"):
#				contents.append(file)
#			file = dir.get_next()
#		for i in range(contents.size()):
##			sound_library[contents[i].get_basename()] = load(str(SFX_PATH,contents[i]))
#			audio_lib[contents[i].get_basename()] = load(str(SFX_PATH,contents[i]))

func load_sfxdb():
	var load_lib = Node.new()
	load_lib.set_script(preload("res://player/sfx/sfx_db.gd"))
	load_lib.parent = self
	add_child(load_lib)
#	print(str("file is:",audio_lib))

func _input(event):
	if input_freeze>0:
		return
	walk_side = Input.get_action_strength("walk_side_r") - Input.get_action_strength("walk_side_l")
	walk_dir = Input.get_action_strength("walk_back") - Input.get_action_strength("walk_forward")
	if event.get_class() == "InputEventMouseMotion":
		var mouse_motion = event.relative*mouse_speed
		rotation.y -= mouse_motion.x

		if mouse_motion.y != 0:
			pitch_value = clamp(pitch_value-mouse_motion.y,pitch_limit.x,pitch_limit.y)
			pitch.rotation.x = pitch_value
			adj_sight(pitch_value)

func adj_sight(pitching):#probably we should simplify/optmize this
	var limits = clamp(pitching, anim_pitch_limits[1], anim_pitch_limits[0])
	var value = inverse_lerp(anim_pitch_limits[0], anim_pitch_limits[1], limits)
	var fin = (value*(anim_arm_y[1]-anim_arm_y[0]))+anim_arm_y[0]
	arms.rect_global_position.y = fin
	
func pro_walk():
	if transition:
		if anim.current_animation == "onair-a2g":
			return
		else:
			transition = false
			anim_new = "idle"
#			print("xidle_ok")
			sfx_new[0] = str("runstop_",ground)
	if vel.length() >8:
		anim_new = "run"
		sfx_new[0] = str("run_",ground)
		if walk_side > dead_zone:
			anim_new = "run_side_r"
		elif walk_side < -dead_zone:
			anim_new = "run_side_l"
	else:
		anim_new = "idle"
#		print("xidle_ok")
		sfx_new[0] = str("runstop_",ground)

func pro_fall():
	if transition:
		if anim.current_animation == "onair-g2a":
			return
		else:
			transition = false
			anim_new = "onair"

	anim_new = "onair"







func status_change(from,to):#status changes from process perspective

	adj_sight(pitch_value)#each status change, we adjust the sight (FP sprite) to clean some issues
						#sort of hardfix, so we'd better keep a look at this


	if from == statedb["walking"]:
		if to == statedb["falling"]:
			
			anim_old = "onair"
			anim_new = "onair"
			anim.current_animation ="onair-g2a"
			anim.seek(0)
			transition = true
	elif from == statedb["falling"]:
		if to == statedb["walking"]:
			anim_old = "onair"
			anim.current_animation ="onair-a2g"
			anim.seek(0)
			anim_new = "idle"
			anim_old = "idle"
			transition = true
		elif to == statedb["climb"]:
			camera.speed = CAMERA_SPD*0.5

			anim.play("onair-grab")
			$anim/headanim.play("onair-grab")
			anim_new = "onair"
			anim_old = "onair"
			transition = true

func pro_debug(delta):
	if Input.is_action_just_pressed("debug_a"):
#		var event = InputEventKey.new()
		_input(InputEventKey.new())
		
		
		sfx_new = ["run_solidground",0]
	elif Input.is_action_just_pressed("debug_b"):
		voice_new = ["exclamation-ah",0]
	elif Input.is_action_just_pressed("debug_c"):
		pass


func _process(delta):
#	get_node("pitch/camera/Label").text = str(pitch_value)
	if input_freeze>0:
		input_freeze -= 1*delta
#	$pitch/camera/Label.text = str(camera.speed)
	pro_debug(delta)
#	$pitch/camera/fpslabel.set_text(str(Engine.get_frames_per_second()))
	var ray_col = $pitch/camera/RayCast.get_collider()
#	if ray_col != null:
#		base.label.text = ray_col.name
#	else:
#		base.label.text = ""

	if camera.speed < CAMERA_SPD:#ensure camera smooth transition(1/2)

#		camera.speed += 100*delta
		camera.speed += 10*delta

	unslide = (abs(walk_side)+abs(walk_dir)) == 0
	
	if status == statedb["walking"]:
		pro_walk()
	elif status == statedb["falling"]:
		pro_fall()
#	elif status == statedb["climb"]:
#		print($anim/headanim.current_animation)
	
	if anim_old != anim_new:
#		print(str("animfrom: ",anim_old, " :to: ",anim_new))
		$anim.play(anim_new)
		anim_old = anim_new
		camera.speed = 0#ensure camera smooth transition(2/2)
	if sfx_new[0] != sfx_old:
		if sfx_new[1] <= 0:
			proc_sound("sfx")
			sfx_new[1] = SFX_DELAY
		else:
			sfx_new[1] -= 1*delta
	if voice_new[0] != voice_old:
		voice_new[1] -= 1*delta
		if voice_new[1] <= 0:
			proc_sound("voice")
	var collisions = get_slide_count()
	if collisions > 0:
		collisions = get_slide_collision(collisions-1).get_collider()
		if collisions.has_meta("killing"):
			get_tree().quit()
	if status != old_status:
		status_change(old_status,status)
		old_status = status

func proc_sound(kind):
	if kind == "sfx":
#		$sfx.stream = sound_library[sfx_new[0]]
		$sfx.stream = audio_lib[sfx_new[0]]
		sfx_old = sfx_new[0]
		$sfx.playing = true
	elif kind == "voice":
		$voice.stream = audio_lib[voice_new[0]]
		voice_old = voice_new[0]
		voice_new[1] = 0
		$voice.playing = true

func get_dir(aim):
	var dir = Vector3() # Where does the player intend to walk to
	dir += walk_dir*aim[2]
	dir += walk_side*aim[0]
	dir.y = 0
	dir = dir.normalized()
	return dir


func phy_climb(delta):
	input_freeze = 0.4

	var y_pos = global_transform.origin.y
#	var step = (climb_target.y-global_transform.origin.y)*0.01
	y_pos += 3*delta
	global_transform.origin.y = y_pos
	if y_pos >= climb_target.y:
		var aim = transform.basis
		arms.visible = true
		vel = vel.linear_interpolate((Vector3(0,0,00)+(aim[2]*-10)),0.9)
		unslide = false
		status = statedb["falling"]

		input_freeze = 0
		_input(InputEventKey.new())#create a fake event to wake up sleeping process (may have miss some key events)

func phy_walk(delta):
	var on_floor = is_on_floor()
	var aim = transform.basis#where I am looking currently
	var dir = get_dir(aim) # Where does the player intend to walk to
	
	var gravity = g
	if unslide and on_floor:
		gravity=0
	vel.y += delta * gravity


	var hvel = vel
	hvel.y = 0

	var target = dir * (MAX_SPEED*speed_mult)
	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL

	hvel = hvel.linear_interpolate(target, accel * delta)
	
	vel.x = hvel.x
	vel.z = hvel.z


	vel = move_and_slide(vel, Vector3(0,1,0),unslide)





	if on_floor:
		fall_timer = fall_delay
	else:
		fall_timer-= 1*delta
		if fall_timer <= 0:
			fall_timer = fall_delay
			status = statedb["falling"]
			sfx_new[0] = str("runstop_"+ground)
		return


	if Input.is_action_pressed("jump"):
		vel.y = JUMP_STR
		status = statedb["falling"]
		voice_new[0] = "voicetough-08"
		sfx_new[0] = str("runstop_"+ground)
	if Input.is_action_just_pressed("action_left"):
		vel = vel.linear_interpolate((Vector3(0,10,00)+(aim[2]*-30)),0.9)
		unslide = false
		walk_dir = 2
		status = statedb["falling"]
		

func phy_fall(delta):
	var old_vel = vel
	var aim = transform.basis#where I am looking currently
	var dir = get_dir(aim) # Where does the player intend to walk to

	vel.y += delta * g

	var hvel = vel
	hvel.y = 0

	var target = dir * MAX_SPEED
	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL

	target = hvel.linear_interpolate(target, 0.1)#while in air, reduce the power of movment
	hvel = hvel.linear_interpolate(target, accel * delta)
	
	vel.x = hvel.x
	vel.z = hvel.z

	vel = move_and_slide(vel, Vector3(0,1,0),unslide)


	if is_on_floor():

		if old_vel.y <= -40:
			voice_new[0] = "voicetough-06"
		elif old_vel.y <= -20:
			voice_new[0] = "voicetough-03"
		status = statedb["walking"]
		sfx_new[0] = str("runstop_",ground)
		return

	if $grab_hook_a.get_collider() != null:
		var no_space =$grab_hook_b.get_overlapping_bodies().size()
		if no_space == 0:
			arms.visible = false
			voice_new[0] = "voicetough-09"
			sfx_new[0] = str("runstop_",ground)
			status = statedb["climb"]
			pitch_value *= 0.1
			pitch.rotation.x = pitch_value
			climb_target = $grab_hook_b.global_transform.origin


func _physics_process(delta):
	if status == statedb["walking"]:
		phy_walk(delta)
	elif status == statedb["falling"]:
		phy_fall(delta)
	elif status == statedb["climb"]:
		phy_climb(delta)



func debug_stuff():
	$pitch/neck/checkcamera.hide()

func resize():
	var y_ratio = base.screen_size.y/base.base_size.y
	arms.rect_scale = Vector2(y_ratio,y_ratio)*1.1
	$pitch/camera/sight.rect_position = base.screen_size/2

func clean_audio(kind):

	if kind == "voice":
		voice_new[0] = ""
		voice_old = ""






func _on_headanim_animation_started(anim_name):
	print(str("hs: ",anim_name))
	pass # Replace with function body.


func _on_anim_animation_started(anim_name):
	print(str("fs: ",anim_name))

